const request = require('supertest');
const httpStatus = require('http-status');
const bcrypt = require('bcryptjs');
const app = require('../../src/app');
const setupTestDB = require('../utils/setupTestDB');
const { User } = require('../../src/models');
const { userOne, insertUsers } = require('../fixtures/user.fixture');
const { userOneAccessToken } = require('../fixtures/token.fixture');

setupTestDB();

describe('Profile routes', () => {
  describe('GET /v1/me', () => {
    test('should return 200 and user profile data', async () => {
      await insertUsers([userOne]);

      const res = await request(app)
        .get('/v1/me')
        .set('Authorization', `Bearer ${userOneAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({ id: expect.anything(), name: userOne.name, email: userOne.email, role: 'user' });
    });

    test('should return 403 if user is not authenticated', async () => {
      await insertUsers([userOne]);

      await request(app)
        .patch('/v1/me/update-password')
        .send()
        .expect(httpStatus.UNAUTHORIZED);
    });
  });

  describe('POST /v1/me/update-password', () => {
    test('should return 200 and reset the password', async () => {
      await insertUsers([userOne]);

      await request(app)
        .patch('/v1/me/update-password')
        .set('Authorization', `Bearer ${userOneAccessToken}`)
        .send({ oldPassword: 'Test123!', newPassword: 'Test234!', newPasswordRetyped: 'Test234!' })
        .expect(httpStatus.OK);

      const dbUser = await User.findById(userOne._id);
      const passwordMatches = await bcrypt.compare('Test234!', dbUser.password);
      expect(passwordMatches).toBe(true);
    });

    test('should return 409 if new password is incorrectly retyped/confirmed', async () => {
      await insertUsers([userOne]);

      await request(app)
        .patch('/v1/me/update-password')
        .set('Authorization', `Bearer ${userOneAccessToken}`)
        .send({ oldPassword: 'Test123!', newPassword: 'test!', newPasswordRetyped: 'test' })
        .expect(httpStatus.CONFLICT);
    });
  });
});
