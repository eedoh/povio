const request = require('supertest');
const httpStatus = require('http-status');
const app = require('../../src/app');
const setupTestDB = require('../utils/setupTestDB');
const { userOne, userTwo, admin, insertUsers } = require('../fixtures/user.fixture');
const { userOneAccessToken, adminAccessToken } = require('../fixtures/token.fixture');

setupTestDB();

describe('User routes', () => {
  describe('GET /v1/user/:userId', () => {
    test('should return 200 and the user object if data is ok', async () => {
      await insertUsers([userOne]);

      const res = await request(app)
        .get(`/v1/user/${userOne._id}`)
        .set('Authorization', `Bearer ${userOneAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      expect(res.body).not.toHaveProperty('password');
      expect(res.body).toEqual({
        id: userOne._id.toHexString(),
        email: userOne.email,
        name: userOne.name,
        likesCount: 0
      });
    });

    test('should allow access to unauthorized users', async () => {
      await insertUsers([userOne]);

      await request(app).get(`/v1/user/${userOne._id}`).send().expect(httpStatus.OK);
    });

    test('should return 404 error if user is not found', async () => {
      await insertUsers([admin]);

      await request(app)
        .get(`/v1/users/${userOne._id}`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NOT_FOUND);
    });
  });

  describe('POST /v1/user/:userId/like', () => {
    test('should return 200 if valid userid is provided, and user has not been already liked by the same person', async () => {
      await insertUsers([userOne, admin]);

      const res = await request(app)
        .post(`/v1/user/${userOne._id}/like`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.OK);
    });

    test('should not allow access to unauthorized users', async () => {
      await insertUsers([userOne]);

      await request(app)
      .post(`/v1/user/${userOne._id}/like`)
      .send()
      .expect(httpStatus.UNAUTHORIZED);
    });

    test('should return 404 error if user is not found', async () => {
      await insertUsers([userOne, admin]);

      await request(app)
        .post(`/v1/user/${userTwo._id}/like`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NOT_FOUND);
    });

    test('should not allow liking the same user multiple times', async () => {
      await insertUsers([userOne, admin]);

      await request(app)
        .post(`/v1/user/${userOne._id}/like`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      await request(app)
        .post(`/v1/user/${userOne._id}/like`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.FORBIDDEN);
    });
  });

  describe('POST /v1/user/:userId/unlike', () => {
    test('should return 200 if valid userid is provided, and user has already like the other person', async () => {
      await insertUsers([userOne, admin]);

      await request(app)
        .post(`/v1/user/${userOne._id}/like`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      await request(app)
        .post(`/v1/user/${userOne._id}/unlike`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.OK);
    });

    test('should not allow access to unauthorized users', async () => {
      await insertUsers([userOne]);

      await request(app)
      .post(`/v1/user/${userOne._id}/unlike`)
      .send()
      .expect(httpStatus.UNAUTHORIZED);
    });

    test('should return 404 error if user is not found', async () => {
      await insertUsers([userOne, admin]);

      await request(app)
        .post(`/v1/user/${userTwo._id}/like`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NOT_FOUND);
    });

    test('should not allow unliking users that have not been liked', async () => {
      await insertUsers([userOne, admin]);

      await request(app)
        .post(`/v1/user/${userOne._id}/unlike`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.FORBIDDEN);
    });
  });
});
