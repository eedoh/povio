const request = require('supertest');
const httpStatus = require('http-status');
const app = require('../../src/app');
const setupTestDB = require('../utils/setupTestDB');
const { userOne, admin, insertUsers } = require('../fixtures/user.fixture');
const { adminAccessToken } = require('../fixtures/token.fixture');

setupTestDB();

describe('Social routes', () => {
  describe('POST /v1/most-liked', () => {
    test('should return 200 if user is authenticated', async () => {
      await insertUsers([userOne, admin]);

      await request(app)
        .get(`/v1/most-liked`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.OK);
    });

    test('should return empty array if no likes have been given', async () => {
      await insertUsers([userOne, admin]);

      const res = await request(app)
        .get(`/v1/most-liked`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual([]);
    });

    test('should contain the user in the collection after user is liked', async () => {
      await insertUsers([userOne, admin]);

      await request(app)
        .post(`/v1/user/${userOne._id}/like`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      const res = await request(app)
        .get(`/v1/most-liked`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      expect(res.body[0]).toEqual({ name: userOne.name, email: userOne.email, likes: 1 });
    });
  });
});
