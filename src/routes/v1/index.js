const express = require('express');
const authRoute = require('./auth.route');
const userRoute = require('./user.route');
const docsRoute = require('./docs.route');
const socialRoute = require('./social.route');
const profileRoute = require('./profile.route');

const router = express.Router();

router.use('/', authRoute);
router.use('/user', userRoute);
router.use('/', docsRoute);
router .use('/', socialRoute);
router.use('/me', profileRoute);

module.exports = router;
