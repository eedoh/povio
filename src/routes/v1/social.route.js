const express = require('express');
const auth = require('../../middlewares/auth');
const socialController = require('../../controllers/social.controller');

const router = express.Router();

router
  .route('/most-liked')
  .get(auth('getUsers'), socialController.getMostLiked);

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Users
 *   description: User management and retrieval
 */

/**
 * @swagger
 * path:
 *  /most-liked:
 *    get:
 *      summary: Get the list of all users ordered by number of likes, in descending order
 *      description: Only admins can retrieve all users.
 *      tags: [Social]
 *      security:
 *        - bearerAuth: []
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  results:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/User'
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 */