const express = require('express');
const auth = require('../../middlewares/auth');
const profileController = require('../../controllers/profile.controller');

const router = express.Router();

router
  .route('/')
  .get(auth(), profileController.getUser);

  router
  .route('/update-password')
  .patch(auth(), profileController.updatePassword);

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Profile
 *   description: Profile management
 */

/**
 * @swagger
 * path:
 *  /me:
 *    get:
 *      summary: Get the logged in user's profile
 *      description: Only authenticated user's are allowed to access this endpoint
 *      tags: [Profile]
 *      security:
 *        - bearerAuth: []
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  results:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/User'
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 */

 /**
 * @swagger
 * path:
 *  /me/update-password:
 *    patch:
 *      summary: Update the logged in user's password
 *      description: Only authenticated user's are allowed to access this endpoint
 *      tags: [Profile]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                oldPassword:
 *                  type: string
 *                newPassword:
 *                  type: string
 *                  format: password
 *                  minLength: 8
 *                  description: At least one number and one letter
 *                newPasswordRetyped:
 *                  type: string
 *                  format: password
 *                  minLength: 8
 *                  description: At least one number and one letter
 *              example:
 *                oldPassword: Test123!
 *                newPassword: Test234!
 *                newPasswordRetyped: Test234!
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:

 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "409":
 *          $ref: '#/components/responses/Conflict'
 */