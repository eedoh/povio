module.exports.authController = require('./auth.controller');
module.exports.userController = require('./user.controller');
module.exports.socialController = require('./social.controller');
module.exports.profileController = require('./profile.controller');
