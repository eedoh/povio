const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { userService, authService } = require('../services');

const getUser = catchAsync(async (req, res) => {
  const user = await userService.getUserById(req.user._id.toString());
  if (!user) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'There was an issue retreiving information from the token');
  }
  res.send(user);
});

const updatePassword = catchAsync(async (req, res) => {
  await authService.updatePassword(req.user._id.toString(), req.body.oldPassword, req.body.newPassword, req.body.newPasswordRetyped);
  
  res.sendStatus(200);
});

module.exports = {
  getUser,
  updatePassword
};
