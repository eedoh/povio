const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { userService, socialService } = require('../services');

const createUser = catchAsync(async (req, res) => {
  const user = await userService.createUser(req.body);
  res.status(httpStatus.CREATED).send(user);
});

const getUser = catchAsync(async (req, res) => {
  const user = await userService.getUserByIdWithLikesCount(req.params.userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  res.send(user);
});

const likeUser = catchAsync(async (req, res) => {
  const user = await socialService.likeUser(req.user._id.toString(), req.params.userId);
  res.send(user);
});

const unlikeUser = catchAsync(async (req, res) => {
  const user = await socialService.unlikeUser(req.user._id.toString(), req.params.userId);
  res.send(user);
});

const getMostLiked = catchAsync(async (req, res) => {
  const user = await socialService.getMostLikedUsers();
  res.send(user);
});

module.exports = {
  createUser,
  getUser,
  likeUser,
  unlikeUser,
  getMostLiked
};
