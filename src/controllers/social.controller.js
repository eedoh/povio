const catchAsync = require('../utils/catchAsync');
const { socialService } = require('../services');


const getMostLiked = catchAsync(async (req, res) => {
  const user = await socialService.getMostLiked(req.user._id, req.params.userId);
  res.send(user);
});

module.exports = {
  getMostLiked
};
