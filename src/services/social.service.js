const httpStatus = require('http-status');
const { User, Like } = require('../models');
const ApiError = require('../utils/ApiError');

const alreadyLiked = async (likerId, likeeId) => {
    let likedUser = await User.findOne({ _id: likeeId }).populate('likesReceived');
    if (!likedUser) throw new ApiError(httpStatus.NOT_FOUND, 'User with the id provided can not be found.')

    return likedUser.likesReceived.filter(element => element.id == likerId).length > 0;
  };

const likeUser = async (likerId, likeeId) => {
    if (likerId === likeeId) throw new ApiError(httpStatus.FORBIDDEN, 'You can\'t like youself.');

    let liked = await alreadyLiked(likerId, likeeId);

    if (!liked) {
        let likedUser = await User.findOne({ _id: likeeId });
        likedUser.likesReceived.push(likerId);
        likedUser.save();

        return "success";
    } else {
        throw new ApiError(httpStatus.FORBIDDEN, 'You already liked this user');
    }
}

const unlikeUser = async (likerId, likeeId) => {
    if (likerId === likeeId) throw new ApiError(httpStatus.FORBIDDEN, 'You can\'t unlike yourself.');

    let liked = await alreadyLiked(likerId, likeeId);

    if (liked) {
        let likedUser = await User.findOne({ _id: likeeId });
        likedUser.likesReceived.pull(likerId);
        likedUser.save();

        return "success";
    } else {
        throw new ApiError(httpStatus.FORBIDDEN, 'You can only unlike users you currently like');
    }
}

const getMostLiked = () => {
    const mostLikedUsers = User.aggregate([
        { $unwind : "$likesReceived" },
        {
            "$group": {
                "_id": "$_id",
                "email": { "$first": "$email" },
                "name": { "$first": "$name" },
                "likes": { "$sum": 1 }
            }
        },
        { "$sort": { "likes": -1 } },
        { $project: { _id: false, email: true, name: true, likes: true } }
    ]);

    return mostLikedUsers;
}

module.exports = {
    likeUser,
    unlikeUser,
    getMostLiked
};
