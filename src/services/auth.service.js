const httpStatus = require('http-status');
const userService = require('./user.service');
const ApiError = require('../utils/ApiError');

/**
 * Login with username and password
 * @param {string} email
 * @param {string} password
 * @returns {Promise<User>}
 */
const loginUserWithEmailAndPassword = async (email, password) => {
  const user = await userService.getUserByEmail(email);
  if (!user || !(await user.passwordMatches(password))) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Incorrect email or password');
  }
  return user;
};

/**
 * Update password
 * @param {string} oldPassword
 * @param {string} oldPasswordRetyped
 * @param {string} newPassword
 * @returns {Promise}
 */
const updatePassword = async (userId, oldPassword, newPassword, newPasswordRetyped) => {
  const user = await userService.getUserById(userId);
    
    if (!user) {
      throw new Error();
    }

    oldPasswordCorrect = await user.passwordMatches(oldPassword);
    if (!oldPasswordCorrect) throw new ApiError(httpStatus.CONFLICT, 'Old password incorrect.')

    if (newPassword !== newPasswordRetyped) throw new ApiError(httpStatus.CONFLICT, 'Password and retyped password do not match.')

    await userService.updateUserById(user.id, { password: newPassword });
};

module.exports = {
  loginUserWithEmailAndPassword,
  updatePassword
};
